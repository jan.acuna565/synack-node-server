const express = require("express");
const axios = require("axios");
var cors = require('cors')
require('dotenv').config();

//create express app
const app = express();

//port at which the server will run
const port = process.env.PORT;
app.use(cors())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
//create end point
app.get("/", async (req, res) => {
  const queryObject = req.url;
  try {
		const response = await axios({
			url: `${process.env.SEARCH_API}${queryObject}`,
			method: "get",
		});
		res.status(200).json(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});

//start server and listen for the request
app.listen(port, () =>
  //a callback that will be called as soon as server start listening
  console.log(`server is listening at http://localhost:${port}`)
);
